#@title Plot{ run: "auto" }
#!pip install mpld3

import matplotlib.pyplot as plt
import matplotlib.cm as cm
import E 
import pandas as pd
from ipywidgets import widgets,interact, interactive
#import mpld3 

#mpld3.enable_notebook()

plt.style.use('fivethirtyeight')

dataset=CH01.data
#dataset=pd.read_pickle('/drive/My Drive/WindData/windIIIC.pkl')
n=1000000
%matplotlib inline

xwidget=widgets.Dropdown(
    options=dataset.columns,
    value=dataset.columns[0],
    description='X:',
    disabled=False,
)

ywidget=widgets.Dropdown(
    options=dataset.columns,
    value=dataset.columns[0],
    description='y:',
    disabled=False,
)

cwidget=widgets.Dropdown(
    options=dataset.columns,
    value=dataset.columns[0],
    description='color:',
    disabled=False,
)

#viridis = cm.get_cmap('viridis', n)

def make_plot(xstr,ystr,cstr):
    x=dataset[xstr][0:n] 
    y=dataset[ystr][0:n]
    color= dataset[cstr][0:n]
    plt.figure(num=0, figsize=(8, 6), dpi=80, facecolor='w', edgecolor='k')
  #  plt.xlim(0,360)
  #  plt.ylim(0,35)
    plt.xlabel(xstr)
    plt.ylabel(ystr)
    plt.scatter(x, y, c=color,  cmap=cm.viridis, marker='o' , facecolors='none')

w = widgets.interactive_output(make_plot, {'xstr': xwidget, 'ystr': ywidget,'cstr': cwidget })

#pbox=widgets.HBox([plt.scatter])
box= widgets.HBox([xwidget, ywidget,cwidget])
display(w,box)