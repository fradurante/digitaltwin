#!/usr/bin/python

import numpy as np

import io
import xml.etree.ElementTree as ET
import pandas as pd
import datetime
import time
import re


from math import exp, log
from inverse import inversefunc
from random import seed, random,gauss    
from scipy.interpolate import Rbf, InterpolatedUnivariateSpline,interp1d
from sklearn import linear_model


def array2string(a):
    string="\n".join((" ".join(('{:4d}'.format(b)) for b in x)) for x in a) 
    return string
def array2stringg(a):
    string=' '.join('{:4d}'.format(n) for n in a)
    return string
def removebrakets(a):
    string=re.sub('\[(.*?)\]', '', a)
    return string
def contactassitance():
    email='f.durante@righi.com'    
    print("File not Found.  Please check the function again. If the problem persists contact "+email)



class Wind:

    def __init__(self):
       
        self.data=None
        self.plot=None
        
    def plotseries(self):    
         
        import matplotlib.pyplot as plt
        ax=plt.plot(self.data)
        self.plot=ax
    
    def generate_wind(self,scale,ndays,Iref):
        ''' The variable "noise" is the turbulent (3s) wind which is added to the Weibull distributed 10 min wind speed. 
            It is assumed that "noise" is normally distributed. a and b are first and zero order parameters that define turbulent wind. 
        '''
        a=0.75
        b=5.7

        sizeseries=6*24*ndays

        normarray=np.zeros(sizeseries)                                                                                                        
        windarray=np.zeros(sizeseries)

        weibull=np.random.rayleigh(scale,sizeseries)

        avewsp=0
        normarray[0]=avewsp

        timestep=600
        timestepstr=str(timestep)+'s'
        timescale=3600./timestep
        idxs=pd.date_range(start='1/1/2000', periods=sizeseries,freq=timestepstr)

        rk=0.85
        ar=exp(log(rk)/(timescale))

        for i in range(1,sizeseries):
            r1=gauss(0,1) 
            normarray[i]=ar*normarray[i-1]+r1

        weibull_cdf=interp1d(np.sort(weibull),np.arange(1, sizeseries+1) / sizeseries,kind='linear',fill_value="extrapolate")
        normarray_cdf=interp1d(np.sort(normarray),np.arange(1, sizeseries+1) / sizeseries, kind='linear',fill_value="extrapolate")
        windarray=inversefunc(weibull_cdf,normarray_cdf(normarray))

        # create dataframe starting from average

        wind=pd.DataFrame({'mean':windarray})
        wind.index=idxs

        wind=wind.resample('3s').mean().interpolate()
        noise=np.random.normal(0,Iref*(a*wind['mean']+b),len(wind))

        wind['gust']=wind['mean']+noise
        wind['gust']=wind['gust'].where(wind['gust']>0,0)
        wind['sigma']=abs((wind['gust']-wind['mean'])).resample('600s').mean().interpolate()
        wind['min']=wind.gust.resample('600s').min().interpolate()
        wind['max']=wind.gust.resample('600s').max().interpolate()
        wind['TI']=wind['sigma']/wind['mean'].resample('600s').mean()
        wind['mean']=wind['mean'].resample('600s').mean()
        wind=wind.drop(columns=['gust']).resample('600s').mean()
        self.data=wind  


class Station:
    '''The mast class defines the methods and attributes for station-like objects like 
    masts, waether stations, point time-series (e.g. output from models) 
    Params: None
    Attributes:
        name (str): The arg is used to provide the mast object with an ID   
        data (dataframe): Pandas dataframe containning the data. Generally not specified.
        easting (float): Easting of the mast or station
        northing (float): Northing of the mast or station
        epsg (string): EPSG georeference code. Default vlue is '4326' (WGS 84, latitude/longitude coordinate system)
    '''
 

    def __init__(self,name='',data=None,mastheight=None,easting=None,northing=None,epsg='4326'):
        self.name=name
        self.easting=easting
        self.northing=northing
        self.epsg=epsg



class Mast(Station):
    '''
    The mast class defines the methods and attributes for mast-like objects    

    Params: None
    
    Attributes:
        name (str): The arg is used to provide the mast object with an ID   
        data (dataframe): Pandas dataframe containning the data. Generally not specified.
        mastheight (float): The maximum height of the mast or station
        easting (float): Easting of the mast or station
        northing (float): Northing of the mast or station
        epsg (string): EPSG georeference code. Default vlue is '4326' (WGS 84, latitude/longitude coordinate system)
    '''

    def __init__(self,name='',data=None,mastheight=None,easting=None,northing=None,epsg='4326'):
        self.data=data
        self.mastheight=mastheight
        self.logger=Logger()
        super().__init__(name,easting,northing,epsg)

    def loadnrgdata(self,filename):
        try:
            fi=open(filename,'r')
        except:
            contactassitance()
            return
        datalines=fi.readlines()
        count=-1
        for line in datalines:
            a=line.split('\t')
            count=count+1
            if len(a)> 0 and a[0]=="Date & Time Stamp":
                break
        try:                
            self.data=pd.read_table(filename,skiprows=count,header=0,index_col=0)
        except:
            contactassitance()
            return
        self.data.index = pd.to_datetime(self.data.index)

    def loadwrfdata(self,filename):
        try:
            fi=open(filename,'r')
        except:
            contactassitance()
            return
        datalines=fi.readlines()
        count=0
        channelnames=[]
        for line in datalines:
            a=line.split()
            
            count=count+1
            if a[0]=='#Channel:':
               channelnames.append(a[1])            
            if len(a)> 0 and a[0]=="#Begindata":
                break
        try:
            self.data=pd.read_csv(filename,skiprows=count,names=channelnames,index_col=0,sep='\t')
        except:
            contactassitance()
            return
        self.data.index = pd.to_datetime(self.data.index)

    def loadwgdata(self,filename):
        '''filename: name of the windographer export (.txt) file'''
        channels={}
        try:            
            self.data=pd.read_csv(filename,skiprows=12, index_col=0,dayfirst=True,parse_dates=True,sep='\t',encoding='iso-8859-1')
        except:
            contactassitance()
            return
        self.data.index = pd.to_datetime(self.data.index)
        for column in self.data.columns:            
            newname=column.replace(' ','')
            newname=newname.replace('@','')           
            newname=removebrakets(newname)
            channels.update({column: newname}) 
        self.data.rename(columns=channels, inplace=True)

class MCP:

	def __init__(self,name='',df1=None,df2=None,shift=0):
		self.name=name
		self.data1=df1.shift(shift)
		self.data2=df2
		self.predictions=None
		self.concdata=None
		self.shift=shift
		self.score=None
		
	def regress(self):
		X=self.data1.to_frame()
		y=self.data2.to_frame()
		nameX=list(X.columns.values)[0]
		namey=list(y.columns.values)[0]

		self.concdata=pd.concat([X,y],join='outer',axis=1).dropna(axis=0, how='any')
		self.concdata.columns = ['X','y']		
		lm = linear_model.LinearRegression( copy_X=True )

		model = lm.fit(self.concdata[['X']],self.concdata[['y']])
		newname=self.concdata.columns[[0]]+'_syntetized_lm'
		newx=np.array(self.data1.dropna(axis=0, how='any')).reshape(-1,1)		
		newidx=self.data1.dropna(axis=0, how='any').index	
		self.predictions = pd.DataFrame(data=lm.predict(newx),index=newidx,columns=newname)
		self.score = lm.score(self.concdata[['X']],self.concdata[['y']])
	def plot(self):
		from bokeh.plotting import figure,  show, output_notebook
		p = figure(plot_width=300, plot_height=300)
		p.scatter(self.concdata['X'],self.concdata['y'], legend="mcp",line_color='red')
		show(p)

class Device:
    def __init__(self,name='',manufacturer='',make=''):
        self.name=name
        self.manufacturer=manufacturer
        self.make=make       

class Sensor(Device):
    def __init__(self,name='',manufacturer='',make='',sensorheight=None,sensortype=None):
        super().__init__(name,manufacturer,make)
        self.sensortype=sensortype        
        self.sensorheight=sensorheight
        self.sensortype=sensortype      

    def addchannel(self,channel):
        self.data=pd.concat([channel.to_frame(), self.data],axis=1)

class Logger(Device):
    def __init__(self,name='',manufacturer='',make='',sensorheight=None,sensortype=None):
        super().__init__(name,manufacturer,make)
        self.sensortype=sensortype        
        self.sensorheight=sensorheight
        self.sensortype=sensortype      

    def addchannel(self,channel):
        self.data=pd.concat([channel.to_frame(), self.data],axis=1)        
        
class Channel:
    mom=None
    def mom(self):
        self.mom=self.groupby(self.index.month).mean().values

class Shear:
    
    shearexponent=None
    def calcshear(self,channellist,method='PL',threshold=0.):
        
        if method=='MEAN':
            v1=channellist[0]
            v2=channellist[1]
            z1=v1.sensorheight
            z2=v2.sensorheight            
            self.shearexponent=(np.log(np.mean(v2.data.values)/np.mean(v1.data.values)))/np.log(z2/z1)

        if method=='INSTANT':                  
            v1=channellist[0]
            v2=channellist[1]            
            v1d=v1.data
            v2d=v2.data            
            v1s=v1d[v1d>threshold].values
            v2s=v2d[v2d>threshold].values            
            z1=v1.sensorheight
            z2=v2.sensorheight
            shearexponentts=np.log(v2s/v1s)/np.log(z2/z1)              
            self.shearexponent=np.mean(shearexponentts[~np.isnan(shearexponentts)])
            
        if method=='MOMM':                          
            v1=channellist[0]
            v2=channellist[1]            
            v1d=v1.data
            v2d=v2.data        
            v1s=v1d
            v2s=v2d            
            z1=v1.sensorheight
            z2=v2.sensorheight            
            mom1=v1d.groupby(v1d.index.month).mean().values
            mom2=v2d.groupby(v2d.index.month).mean().values   
            months=[(datetime.date(2000, i,1).strftime('%b')) for i in v1d.groupby(v1d.index.month).groups.keys()]
            self.shearexponent=[months,(np.log(mom2/mom1)/np.log(z2/z1)).flatten()]
                        
        if method=='MOMMHOD':                          
            v1=channellist[0]
            v2=channellist[1]            
            v1d=v1.data
            v2d=v2.data        
            v1s=v1d
            v2s=v2d            
            z1=v1.sensorheight
            z2=v2.sensorheight            
            mom1=v1d.groupby([v1d.index.month,v1d.index.hour]).mean().values                        
            mom2=v2d.groupby([v2d.index.month,v2d.index.hour]).mean().values   
            keys=v1d.groupby([v1d.index.month,v1d.index.hour]).groups.keys()
            self.shearexponent=[keys,(np.log(mom2/mom1)/np.log(z2/z1)).flatten()]         
            
class SUP:
    def __init__(self,tab1,mast1,tab2,mast2,vsensorname,dsensorname):
        
        self.name=None
        self.sup=[]
        
        self.ndbins1=tab1.ndbins
        self.dbinwidth1=tab1.dbinwidth
        self.vfrequencies1=tab1.vfrequencies
        self.dfrequencies1=tab1.dfrequencies
        self.data1=mast1.data
        self.vsensorname=vsensorname
        self.dsensorname=dsensorname

        self.ndbins2=tab2.ndbins
        self.dbinwidth2=tab2.dbinwidth
        self.vfrequencies2=tab2.vfrequencies
        self.dfrequencies2=tab2.dfrequencies
        self.data2=mast2.data

    def calcsup(self):
  
        v1=self.data1[self.vsensorname]
        v2=self.data2[self.vsensorname]
        d1=self.data1[self.dsensorname]
        d2=self.data2[self.dsensorname]
        for sec in np.arange(0,360,self.dbinwidth1):            
            v1d=v1[(d1>sec)&(d1<sec+self.dbinwidth1)]
            v2d=v2[(d1>sec)&(d1<sec+self.dbinwidth1)]
            self.sup.append(np.mean(v2d/v1d))
            

class Tab:
    def __init__(self,name='',form='o'):

        self.form="o"
        self.tab=None
        self.tabd=None
        self.maxv=50
        self.vbins=range(0,self.maxv)
        self.ndbins=16
        self.vbinswidth=1.0
        self.height=0
        self.dbinwidth=360./self.ndbins
        self.vfrequencies=np.zeros((self.maxv,self.ndbins))
        self.dfrequencies=np.zeros((self.ndbins))
        self.latitude=0
        self.longitude=0
        self.scale=1.
        self.offset=0.
        self.dbins=np.arange(0,360.,360./self.ndbins)
    
    def loadtab(self,tabfile):
        self.vbins=np.loadtxt(tabfile, usecols=range(0,1), skiprows=4)
        self.vfrequencies=np.loadtxt(tabfile, usecols=range(1,self.ndbins+1), skiprows=4)/1000.
        self.dfrequencies=np.loadtxt(tabfile, usecols=range(0,self.ndbins), skiprows=3)[0]/100.
        self.vbinwidth=self.vbins[-1]/len(self.vbins)
        self.latitude, self.longitude, self.height=np.loadtxt(tabfile, usecols=range(0,3),skiprows=1)[0]
        self.ndbins, self.scale, self.offset=np.loadtxt(tabfile, usecols=range(0,3),skiprows=2)[0]

    def maketab(self,vseries,dseries):
        y=dseries
        x=vseries
        maxspeedbin=int(max(x)+1)
        speedbins = np.linspace(0, maxspeedbin, maxspeedbin+1)                
        dirbins = np.linspace(-self.dbinwidth/2.,(360-self.dbinwidth/2.) , 360./self.dbinwidth+1)                        
        digitized_speed=np.digitize(x,speedbins)-1
        digitized_dir=np.digitize(y.where(y<(360-self.dbinwidth/2.),y-360),dirbins)-1

        tabdata=np.zeros((len(speedbins),len(dirbins)-1))
        dirfreq=np.zeros(len(dirbins)-1)
        

        for j in range(0,len(dirbins)-1):
                freqd=digitized_dir==j
                for i in range(0,len(speedbins)):                                                               
                    freqs=digitized_speed==i

                    tabdata[i,j] = float(np.sum(freqd*freqs))
     
                dirfreq[j]=sum((tabdata[:,j]))

        
        for i in range(0,  len(tabdata[:,0])-1 ):
            for j in range(0,len(tabdata[0,:])):
                if np.sum(tabdata[i,:])!=0:
                    self.vfrequencies[i,j]=tabdata[i,j]/np.sum(tabdata[i,:])
                else:
                    self.vfrequencies[i,j]=0.

        self.dfrequencies=dirfreq/np.sum(dirfreq)


    def writetab(self,foname):

        fo=open(foname,'w')
        fo.write('created by maketab.py \n')
        fo.write('{:3.3f}'.format(self.latitude)+' '+'{:3.3f}'.format(self.longitude)+' '+'{:3.1f}'.format(self.height)+'\n')
        fo.write(str(self.ndbins)+' '+str(self.scale)+' '+str(self.offset)+'\n')


        if self.form=="o":
            for j in range(0,self.ndbins):
                    fo.write('{:6d}'.format(int(self.dfrequencies[j])  ))
            fo.write("\n")
            for i in range(0,len(self.vfrequencies[:,0])):
                    ii=i+1
                    fo.write('{:6d}'.format( int(ii) ))
                    for j in self.vfrequencies[i,:] :
                            fo.write('{:6d}'.format(int(j)))
                    fo.write("\n")
            fo.close()

        if self.form=="f":
            for j in range(0,self.ndbins):
                    fo.write('{:7.2f}'.format((self.dfrequencies[j]/np.sum(self.dfrequencies[:]) )*100.   ))
            fo.write("\n")

            for i in range(0,len(self.vfrequencies[:,0])):
                    ii=i+1
                    fo.write('{:6d}'.format(ii))
            
                    for j in range(0,len(self.dfrequencies[:])) :
                            if self.vfrequencies[i,j] != 0.:
                                fo.write('{:8.2f}'.format((self.vfrequencies[i,j]/np.sum( self.vfrequencies[:,j])*1000.)))
                            else:
                                fo.write('{:8.2f}'.format(0.))
                    fo.write("\n")
            fo.close()

class WTG:
    vbins=50
    pc=np.zeros([2,vbins])
    pfc=None

    def loadpow(self,wtgfile):
        self.pc=np.loadtxt(wtgfile, usecols=range(0,2),unpack=True)
        self.pcf = interp1d(self.pc[0], self.pc[1])

    def interpolatepc(self,zoom):
        newsize=zoom*self.vbins+1
        pci=np.zeros([2,newsize])
        np.resize(self.pc,(2,newsize))
        vbinsi=np.linspace(self.pc[0][0],self.vbins,newsize)
        pcf = interp1d(self.pc[0], self.pc[1])
        pci[0]=vbinsi
        pci[1]=pcf(vbinsi)
        self.pc=pci 

    def shiftpc(self,shift):
        self.pc[0]=self.pc[0]-shift

    def loadwtg(self,wtgfile):
        tree = ET.parse(wtgfile)
        root = tree.getroot()
        count=0
        pcc=np.zeros([self.vbins,2])
        for i in range(0,self.vbins):
            for child in root.iter('DataPoint'):
                if i ==int(float((child.attrib['WindSpeed']))):
                    pcc[i,0]=float((child.attrib['WindSpeed']))
                    pcc[i,1]=float((child.attrib['PowerOutput']))
                    count=count
                     
        self.pc[0]=range(0,self.vbins)
        self.pc[1]=np.stack(pcc[:,1])
        self.pcf = interp1d(self.pc[0], self.pc[1])

class Energy:

    def __init__(self,tab,wtg):
        self.tab=Tab()
        self.wtg=WTG()
        self.nsectors=16
        self.omnidirectionalgross=None
        self.sumofallsectorgross=None
        self.allsectorgross=np.zeros(self.nsectors)
        
    def aligntabpcbins(self):
        xt=self.tab.vbins
        xpc=self.wtg.pc[0]


    def calcenergy(self):
        for dbin in range(0,self.nsectors):
            self.allsectorgross[dbin]=np.sum(self.tab.vfrequencies[:-1,dbin]*self.wtg.pcf(self.wtg.pc[0][1:]-self.tab.vbinswidth/2.))*self.tab.dfrequencies[dbin]
        self.sumofallsectorgross=np.sum(self.allsectorgross)*24*365/1000.

